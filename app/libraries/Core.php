<?php
//Url mapping class

class Core {

    protected $CurrentController = 'Pages';

    protected $CurrentMethod = 'index';

    protected $params = [];

    public  function __construct()
    {
        $url = $this->getUrl();

        if(file_exists('../app/controllers/'. ucwords($url[0]. '.php'))){

            $this->CurrentController = ucwords($url[0]);

            unset($url[0]);
        }

        require_once '../app/contollers/'. $this->CurrentController .'.php';

        $this->CurrentController = new $this->CurrentController;

        if(isset($url[1])){

            if(method_exists($this->CurrentController , $url[1])){

                $this->CurrentMethod = $url[1];

                unset($url[1]);
            }
        }

        $this->params = $url ? array_values($url) : [];

        call_user_func_array([$this->CurrentController, $this->CurrentMethod],$params);
    }

    public function getUrl(){

        $url = $_GET['url'];

        $url = rtrim($url , '/');

        $url = filter_var($url , FILTER_SANITIZE_STRING);

        $url = explode($url , '/');
        
        return $url;

    }
}